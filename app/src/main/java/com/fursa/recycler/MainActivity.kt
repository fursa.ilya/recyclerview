package com.fursa.recycler

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import com.fursa.recycler.adapter.SectionedAdapter
import com.fursa.recycler.data.DataGenerator
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    val sectionedAdapter = SectionedAdapter()
    sectionedAdapter.setItems(DataGenerator.items)

    recyclerView.apply {
      adapter = sectionedAdapter
      layoutManager = LinearLayoutManager(context)
    }
    (recyclerView.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
  }
}