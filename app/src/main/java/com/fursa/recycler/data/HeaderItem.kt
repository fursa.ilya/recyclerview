package com.fursa.recycler.data

data class HeaderItem(
    val title: String,
    val items: List<TextItem>,
    var expanded: Boolean
): ListItem