package com.fursa.recycler.data

object DataGenerator {
    val items = mutableListOf<ListItem>()
    val first = mutableListOf<TextItem>()
    val second = mutableListOf<TextItem>()
    val last = mutableListOf<TextItem>()

    init {

        for (i in 1..3) {
            first.add(TextItem("Title 1", "Content 1"))
        }

        for (i in 1..2) {
            second.add(TextItem("Second", "Lorem ipsum dolor sit ament"))
        }

        for (i in 1..2) {
            last.add(TextItem("Title 2", "Content 2"))
        }
        items.add(HeaderItem("Header item 1", first, true))
        items.add(HeaderItem("Header item 1", second, true))
        items.add(HeaderItem("Header item 2", last, true, ))

    }

}

