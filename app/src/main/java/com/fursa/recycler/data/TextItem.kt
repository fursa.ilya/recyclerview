package com.fursa.recycler.data

data class TextItem(
    val title: String,
    val content: String,
): ListItem