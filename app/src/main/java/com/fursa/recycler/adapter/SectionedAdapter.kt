package com.fursa.recycler.adapter

import android.graphics.Color
import android.transition.AutoTransition
import android.transition.TransitionManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.fursa.recycler.R
import com.fursa.recycler.data.HeaderItem
import com.fursa.recycler.data.ListItem
import com.fursa.recycler.data.TextItem
import kotlinx.android.synthetic.main.item_header.view.*
import kotlinx.android.synthetic.main.item_text.view.*
import java.lang.IllegalArgumentException

class SectionedAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var items = mutableListOf<ListItem>()

    fun setItems(items: MutableList<ListItem>) {
        this.items = items
        notifyDataSetChanged()
    }

    class HeaderItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
         fun bind(item: HeaderItem) = with(itemView) {
            textViewHeader.text = item.title
        }
    }
    class TextItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
         fun bind(item: TextItem) = with(itemView) {
             textViewTitle.text = item.title
             textViewContent.text = item.content
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when(viewType) {
            TYPE_HEADER -> {
                val headerView = LayoutInflater.from(parent.context).inflate(R.layout.item_header, parent, false)
                return HeaderItemViewHolder(headerView)
            }
            TYPE_TEXT -> {
                val textView = LayoutInflater.from(parent.context).inflate(R.layout.item_text, parent, false)
                return TextItemViewHolder(textView)
            }
            else -> throw IllegalArgumentException()
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items[position]
        when(holder) {
            is HeaderItemViewHolder -> {
                holder.bind(item as HeaderItem)
                holder.itemView.setOnClickListener {
                    if(item.expanded) {
                        items.addAll(position + 1, item.items)
                        notifyItemRangeInserted(position + 1, item.items.size)
                    }
                    else {
                        items.removeAll(item.items)
                        notifyItemRangeRemoved(position + 1, item.items.size)
                    }
                    item.expanded = !item.expanded

                }
            }
            is TextItemViewHolder -> {
                holder.bind(item as TextItem)
            }
            else -> throw IllegalArgumentException()
        }
    }

    override fun getItemCount(): Int {
        return items.count()
    }

    override fun getItemViewType(position: Int): Int {
        val item = items[position]
        return when(item) {
            is HeaderItem -> TYPE_HEADER
            is TextItem -> TYPE_TEXT
            else -> throw IllegalArgumentException()
        }
    }

    companion object {
        const val TYPE_HEADER = 0
        const val TYPE_TEXT = 1
    }
}